yowsup (3.2.3-2) unstable; urgency=medium

  * debian/control: Add missing dependency on python3-consonance for
    python3-yowsup. Closes: #959238
  * Bump debhelper-compat to (= 13)
  * Bump Standards-Version to 4.5.0. No changes required
  * debian/control: Document Rules-Requires-Root field as no
  * Declare autopkgtest as superficial. Closes: #969883

 -- Josue Ortega <josue@debian.org>  Tue, 13 Oct 2020 19:41:28 -0600

yowsup (3.2.3-1) unstable; urgency=medium

  * New upstream release (3.2.3):
    - Update Dependencies
    - Refresh patches
    - Remove debian/patches/00-fix-python37-compat.patch. Patch has been
      appplied by upstream.
    - Remove debian/patches/00-fix-protocol-tree-node-import.patch. Target
      file is not longer distributed
  * Drop python-yowsup-common. No longer needed
  * Add patch that upgrades Android environment MD5 Class and version.
    Closes: #840005
  * Remove http://mcclist.com from manpage. Closes: #912698

 -- Josue Ortega <josue@debian.org>  Thu, 05 Sep 2019 15:29:02 -0600

yowsup (2.5.7-5) unstable; urgency=medium

  * Drop Python 2 package. Thangks to Steve Langasek
    <steve.langasek@canonical.com>
  * Update package to use debhelper-compat (= 12)
  * Add Debian CI tests
  * Bump Standards-Version to 4.4.0. No changes required

 -- Josue Ortega <josue@debian.org>  Wed, 04 Sep 2019 17:40:42 -0600

yowsup (2.5.7-4) unstable; urgency=medium

  [ Josue Ortega ]
  * Fix compatibility with Python 3.7 (Closes: #904653).
  * Bump Standards-Version to 4.1.5, no changes required.
  * debian/rules: Change PYBUILD_AFTER_INSTALL in order to fix
    yowsup-cli executable installation.
  * Bumps Standards-Version to 4.2.1, no changes needed.
  * debian/rules:
    - Fix Description paragraph alignment to avoid errors
      at parsing dots (.).
    - Update yowsup-cli dependency python packages to use python3 version.

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field

 -- Josue Ortega <josue@debian.org>  Fri, 05 Oct 2018 14:52:27 -0600

yowsup (2.5.7-3) unstable; urgency=medium

  * Adds missing Breaks+Replaces package relationship with python-yowsup
    (<< 2.5.7-1~), (Closes: #889719).

 -- Josue Ortega <josue@debian.org>  Mon, 26 Feb 2018 20:04:16 -0600

yowsup (2.5.7-2) unstable; urgency=medium

  * Upload to unstable (2.5.7-2).
  * debian/control: Updates VCS fields to salsa.debian.org.

 -- Josue Ortega <josue@debian.org>  Mon, 05 Feb 2018 20:25:35 -0600

yowsup (2.5.7-1) experimental; urgency=medium

  * New upstream release (v2.5.7).
  * Adds patch: '00-fix-compat-six.patch' in order to fix compatibility
    with python-six >= 1.11 (Closes: #884102)
  * Disables check of build dependencies by setuptools at buildtime.
  * Adds python3 version of the package
  * Adds python-yowsup-common package to share common files among
    python2 and python3 version of the package.
  * Adds patch: 00-fix-protocol-tree-node-import.patch
  * Bumps Standards-Version to 4.1.3:
    - Changes Format field at debian/copyright to use HTTPS version.
  * debian/copyright: Updates copyright holders for debian/*.
  * Bumps compat level to 11.

 -- Josue Ortega <josue@debian.org>  Sat, 06 Jan 2018 13:18:33 -0600

yowsup (2.5.2-1) experimental; urgency=medium

  * New upstream release (2.5.2).
  * debian/control: Bumps python-axolotl dependency version (0.1.35 -> 0.1.39).

 -- Josue Ortega <josue@debian.org>  Sat, 15 Apr 2017 19:11:05 -0600

yowsup (2.5.0~git20160904.d69c1ff-1) unstable; urgency=medium

  * New upstream release:
    - Adds patch to avoid wrong installation of yowsup data.
  * debian/control:
    - Updated python-axolotl version to 0.1.35 (Closes: #835697).
    - Adds python-axolotl and python-dateutil as dependencies in
      python-yowsup package.

 -- Josue Ortega <josue@debian.org>  Sun, 04 Sep 2016 14:53:07 -0600

yowsup (2.4.103-3) unstable; urgency=medium

  * New maintainer. Closes( #833486)

 -- Josue Ortega <josue@debian.org>  Mon, 08 Aug 2016 19:42:08 -0600

yowsup (2.4.103-2) unstable; urgency=medium

  * QA upload.
  * Set QA Group as maintainer. (see #833486)

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 06 Aug 2016 14:31:24 -0300

yowsup (2.4.103-1) unstable; urgency=medium

  * New upstream release.
  * debian/control: bumped Standards-Version to 3.9.8.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 08 May 2016 01:30:18 -0300

yowsup (2.4.102-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
      - Bumped Standards-Version to 3.9.7.
      - Changed from cgit to git in Vcs-Browser field.
      - Updated the Vcs-* fields to use https instead of http and git.
  * debian/copyright:
      - Added rights for genallman.sh.
      - Updated the upstream copyright years.
  * debian/manpage/:
      - Updated genallman.sh to version 0.3.
      - Updated the manpage (added the -E option).
  * debian/watch: bumped to version 4.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 04 Apr 2016 09:31:50 -0300

yowsup (2.4.48-1) unstable; urgency=medium

  * New upstream release. (Closes: #810473)
  * debian/clean: added to remove files used by upstream to
      build from source code.
  * debian/copyright: updated the packaging copyright years.
  * debian/gbp.conf: not used by me... Removed.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 09 Jan 2016 16:11:56 -0200

yowsup (2.4-1) unstable; urgency=medium

  * New upstream release.
  * debian/manpage/: updated the manpage.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 09 Sep 2015 21:28:05 -0300

yowsup (2.3.185-1) unstable; urgency=medium

  * New upstream release.
  * debian/python-yowsup.docs: added to install the upstream file README.md.
  * debian/yowsup-cli.docs: added to install the upstream file README.md.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 08 Aug 2015 16:04:42 -0300

yowsup (2.3.167-1) unstable; urgency=medium

  * New upstream release.
  * debian/manpage: updated the manpage.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Thu, 23 Jul 2015 17:52:49 -0300

yowsup (2.3.124-1) unstable; urgency=medium

  * New upstream release.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 13 Jul 2015 10:00:37 -0300

yowsup (2.3.123-1) unstable; urgency=medium

  * New upstream release.
  * debian/manpage/: fixed a typo in manpage.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 28 Jun 2015 13:29:13 -0300

yowsup (2.3.84-1) unstable; urgency=medium

  * New upstream release.
  * debian/control: improved the long description.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Thu, 18 Jun 2015 10:48:18 -0300

yowsup (2.2.78+git20150511.b3cdb1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control: added a relationship for python-axolotl in Build-Depends
      field.
  * debian/copyright:
      - yowsup/common/http/httpproxy.py added as MIT.
      - Explicit upstream declaration about GPL-3+ (setup.py file). Changed
        the general licensing from GPL-3 to GPL-3+.
  * debian/manpage/: removed the 'old-yowsup' directory.
  * debian/patches/: all patches removed. The upstream source code has the
      fixes.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 11 May 2015 10:22:12 -0300

yowsup (2.2.78-1) unstable; urgency=medium

  * New upstream release. (Closes: #773005, #775835)
  * debian/clean: removed. The upstream fixed the source code.
  * debian/control:
      - Added python-axolotl, python-dateutil, python-pil and python-setuptools
        to Build-Depends field.
      - Removed python-dateutil from Depends field in yowsup-cli binary.
      - Updated the Vcs-Browser field.
  * debian/copyright:
      - Changed the main license because the upstream is using GPL-3 now.
      - Updated the copyright years for packaging and upstream source code.
  * yowsup-cli.install: useless now. Removed.
  * yowsup-cli.examples: useless now. Removed.
  * debian/man/:
      - Renamed the directory to manpage.
      - Renamed file header.txt to yowsup-cli.header.
      - Using genallman.sh instead of genman.sh.
      - Updated all manpage (the upstream did several changes in yowsup-cli).
  * debian/patches/:
      - Added a patch (fix-ssl) to avoid a SSL certificate verify failed on
        Python 2.7.9. Thanks a lot to Tarek Galal (upstream).
      - Removed some patches unnecessary for new upstream version:
         ~ add-missing-variable
         ~ add-PictureClient
         ~ add-the-tokenmap
         ~ add-whatsapp-auth-v2
         ~ update-bintreenode
         ~ update-connection-manager
         ~ update-protocoltreenode
         ~ update-the-interface-messages
         ~ update-user-agent
  * debian/README.Debian: updated.
  * debian/rules: using pybuild now.
  * debian/yowsup-cli.dirs: created to provide /usr/bin.
  * debian/yowsup-cli.manpages: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 29 Apr 2015 14:48:27 -0300

yowsup (0.0~git20140314.938cf1-3) unstable; urgency=medium

  * Upload to unstable.
  * Added some patches, provided by the upstream (thanks to Tarek Galal),
      to make the program compliant with the WhatsApp version 2.11.432
      (or later), released at November 2014. This version changed several
      things in WhatsApp, including the authentication process. The
      patches added are necessary to provide the connection and basic
      activities only. The list of patches:
         - add-missing-variable
         - add-PictureClient
         - add-the-tokenmap
         - add-whatsapp-auth-v2
         - update-bintreenode
         - update-connection-manager
         - update-protocoltreenode
         - update-the-interface-messages
         - update-user-agent
         - yowsup-cli (updated only; already in Debian)
  * debian/watch: updated. Now, the upstream is using tags in GitHub.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 02 Dec 2014 22:51:22 -0200

yowsup (0.0~git20140314.938cf1-2) experimental; urgency=medium

  * debian/control:
      - Added dh-python to Build-Depends field.
      - Updated Standards-Version to 3.9.6.
  * debian/watch: added a fake site to explain about the current
      status of the original upstream homepage.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 18 Nov 2014 09:00:10 -0200

yowsup (0.0~git20140314.938cf1-1) unstable; urgency=low

  * Initial release (Closes: #752201)

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 20 Jun 2014 16:51:58 -0300
