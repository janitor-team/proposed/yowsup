Source: yowsup
Section: python
Priority: optional
Maintainer: Josue Ortega <josue@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-axolotl (>= 0.2.2),
               python3-pil,
               python3-setuptools,
               python3-consonance (>= 0.1.2),
               python3-six,
			   python3-appdirs,
			   python3-protobuf (>= 3.6.0),
               python3-tqdm,
			   python3-requests
Standards-Version: 4.5.0
Homepage: https://github.com/tgalal/yowsup
Vcs-Git: https://salsa.debian.org/debian/yowsup.git
Vcs-Browser: https://salsa.debian.org/debian/yowsup
Rules-Requires-Root: no

Package: python3-yowsup
Architecture: all
Depends: ${misc:Depends},
 ${python3:Depends},
 python3-appdirs,
 python3-libxml2,
 python3-axolotl (>= 0.2.2),
 python3-consonance (>= 0.1.2),
Breaks: python-yowsup-common (<< 3.2.3-1~)
Replaces: python-yowsup-common (<< 3.2.3-1~)
Description: Python 3 library to implement a WhatsApp client
 WhatsApp Messenger is a cross-platform mobile messaging app
 which allows you to exchange messages, via Internet, without
 having to pay for SMS, using a mobile phone.
 .
 In addition to basic messaging, WhatsApp users can create
 groups, send each other unlimited images, video and audio
 media messages.
 .
 Yowsup is a cross platform Python library that enable to do all
 the previous in your own app. Yowsup allows you to login and use
 the WhatsApp service, providing all capabilities of an official
 WhatsApp client, allowing to create a full-fledged custom
 WhatsApp client.
 .
 python3-yowsup has these features:
    - Registration.
    - Text send/receive.
    - Encryption of messages.
    - Media send/receive (images, videos, audio, location,
      contact cards).
    - Groups(create, leave, join, update picture, updating
      subject).
    - Fetch user profile picture.
    - Fetch user status.
    - Set profile picture.
    - Set group icon.
    - Set status and others.
 .
 The package yowsup-cli is a good example of the python-yowsup
 implementation.

Package: yowsup-cli
Section: net
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
         python3-yowsup,
         python3-protobuf (>=3.6.0),
         python3-pil,
Description: command line tool that acts as WhatsApp client
 WhatsApp Messenger is a cross-platform mobile messaging app which allows
 you to exchange messages, via Internet, without having to pay for SMS,
 using a mobile phone.
 .
 In addition to basic messaging, WhatsApp users can create groups, send
 each other unlimited images, video and audio media messages.
 .
 yowsup-cli is a command line program, based in python-yowsup library, that
 allows you to login and use the WhatsApp service, providing all capabilities
 of an official WhatsApp client, as encryption of messages. This program can
 be used for multiple purposes as e.g. to receive messages from network servers
 or appliances, notifying about issues, via direct command or by special agents
 (Zabbix, Nagios, iwatch, portsentry, etc.).
 .
 This package is a good example of the python-yowsup library implementation.
